/*
Author: Adam Drozdek
Modified by: de Freitas, P.C.P
Description - Binary Search Tree
*/
#include<iostream>
using namespace std;
class BSTNode {
public:
	BSTNode() {
		left = right = 0;
	}
	BSTNode(const int& e, BSTNode *l = 0, BSTNode *r = 0) {
		el = e; left = l; right = r;
	}
	int el;
	BSTNode *left, *right;
};

class BST {
public:
	BST() {
		root = 0;
	}
	~BST() {
		clear();
	}
	void clear() {
		clear(root);
		root = 0;
	}
	bool isEmpty() const {
		return root == 0;
	}
	void preorder() {
		preorder(root);
	}
	void inorder() {
		inorder(root);
	}
	void postorder() {
		postorder(root);
	}
	void insert(int el) {
		BSTNode *p = root, *prev = 0;
		while (p != 0) {  // find a place for inserting new node;
			prev = p;
			if (el < p->el)
			{
				p = p->left;
			}
			else
			{
				p = p->right;
			}
		}
		if (root == 0) {   // tree is empty;
			root = new BSTNode(el);
		}
		else if (el < prev->el) {
			prev->left = new BSTNode(el);
		}
		else {
			prev->right = new BSTNode(el);
		}
	}

	int search(int el)
	{
		while (root != 0)
		{
			if (el == root->el)
			{
				return root->el;
			}
			else if (el < root->el)
			{
				root = root->left;
			}
			else
			{
				root = root->right;
			}
		}
		return 0;
	}

	void deleteByMerging(BSTNode*&);
	void findAndDeleteByMerging(const int&);
private:
	BSTNode* root;
	void clear(BSTNode *p) 
	{
		if (p != 0) {
			clear(p->left);
			clear(p->right);
			delete p;
		}
	}
protected:
	void preorder(BSTNode*);
	void inorder(BSTNode*);
	void postorder(BSTNode*);
};

void BST::inorder(BSTNode *p) {
	if (p != 0) {
		inorder(p->left);
		cout << p->el << ' ';
		inorder(p->right);
	}
}

void BST::preorder(BSTNode *p) {
	if (p != 0) {
		cout << p->el << ' ';
		preorder(p->left);
		preorder(p->right);
	}
}

void BST::postorder(BSTNode* p) {
	if (p != 0) {
		postorder(p->left);
		postorder(p->right);
		cout << p->el << ' ';
	}
}

void BST::deleteByMerging(BSTNode*& node) {
	BSTNode *tmp = node;
	if (node != 0) {
		if (!node->right)           // node has no right child: its left
			node = node->left;     // child (if any) is attached to its parent;
		else if (node->left == 0)   // node has no left child: its right
			node = node->right;    // child is attached to its parent;
		else {                      // be ready for merging subtrees;
			tmp = node->left;      // 1. move left
			while (tmp->right != 0)// 2. and then right as far as possible;
				tmp = tmp->right;
			tmp->right =           // 3. establish the link between the
				node->right;        //    the rightmost node of the left
									//    subtree and the right subtree;
			tmp = node;            // 4.
			node = node->left;     // 5.
		}
		delete tmp;                 // 6.
	}
}

void BST::findAndDeleteByMerging(const int& el) {
	BSTNode *node = root, *prev = 0;
	while (node != 0) {
		if (node->el == el)
			break;
		prev = node;
		if (el < node->el)
			node = node->left;
		else node = node->right;
	}
	if (node != 0 && node->el == el)
		if (node == root)
			deleteByMerging(root);
		else if (prev->left == node)
			deleteByMerging(prev->left);
		else deleteByMerging(prev->right);
	else if (root != 0)
		cout << "el " << el << " is not in the tree\n";
	else cout << "the tree is empty\n";
}

int main()
{
	BST bst;
	bst.insert(1);
	bst.insert(2);
	bst.insert(3);
	bst.insert(4);
	bst.insert(5);
	bst.insert(6);
	bst.insert(7);
	bst.preorder();
	cout << endl;
	bst.postorder();
	cout << endl;
	bst.inorder();
	cout << endl;
	bst.findAndDeleteByMerging(4);
	bst.preorder();
	cout << endl;
	cout << bst.search(4) << endl;
	cout << bst.search(7) << endl;
	return 0;
}