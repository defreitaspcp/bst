Árvore binária de busca
--
Uma árvore binária de busca é uma estrutura de dados de árvore binária baseada em nós, onde todos os nós da subárvore esquerda possuem um valor numérico inferior ao nó raiz e todos os nós da subárvore direita possuem um valor superior ao nó raiz.

Elementos
--
Nós - são todos os itens guardados na árvore;
Raiz - é o nó do topo da árvore;
Filhos - são os nós que vem depois dos outros nós;
Pais - são os nós que vem antes dos outros nós;
Folhas - são os nós que não têm filhos; são os últimos nós da árvore.

Complexidade
--
A complexidade das operações sobre ABB depende diretamente da altura da árvore.
Uma árvore binária de busca com chaves aleatórias uniformemente distribuídas tem altura O(log n).
No pior caso, uma ABB poderá ter altura O(n). Neste caso a árvore é chamada de árvore zig-zag e corresponde a uma degeneração da árvore em lista encadeada.
Em função da observação anterior, a árvore binária de busca é de pouca utilidade para ser aplicada em problemas de busca em geral. Daí o interesse em árvores balanceadas, cuja altura seja O(log n) no pior caso

Principais operações
--
Busca,
Inserção,
Remoção.

Referência
--
[1] Árvore Binária de Busca, Wikipedia, https://pt.wikipedia.org/wiki/%C3%81rvore_bin%C3%A1ria_de_busca